<?php
namespace console\controllers;

use \Yii;
use yii\console\Controller;
use common\models\Common;
 
/**
 * Cron controller
 */
class McController extends Controller {
   
    public function actionSavetodb()
    {
        $modelCommon = new Common();
        $values = array();
        $fields = array('date','email','status');
        $err_txt ="Invalid email addresses: ". PHP_EOL;
        $e = false;
        $merge_vars = array('FNAME' =>'-', 'LNAME' =>'-');
        $filepath = '/var/www/html/mailchimp_yii_console/console';
        $file_err_save = $filepath.'/logs/err_save.txt';
        $file_err_mailchimp = $filepath.'/logs/err_mailchimp.txt';
        $file_json = $filepath.'/data/data.json';
        $status ='';
        $user= array();
        $err_mailchimp = array();
                
        $ls = \Yii::$app->mailchimp->getLists();
        $listID = ($ls['lists'][0]['id']);// typically, it's the first one, if you have only one campaign on MailChimp or your apikey is referred at a specific campaign.
        
        
        try{//checks if JSON files are written
            $string = file_get_contents($file_json);
            $json_a = json_decode($string, true);

            if(is_array($json_a) && !empty($json_a)){// runs only if the array is not empty
                foreach($json_a['users'] as $k=>$v){
                    if (filter_var($v['email'], FILTER_VALIDATE_EMAIL)) {
                        $user = $modelCommon->findOneWhere('mailchimp', 'email', $v['email']);
                        if(!$user){
                            array_push($values,array($v['date'],$v['email'],1));// inserts only new email addresses into the db
                            $status = \Yii::$app->mailchimp->updateMember($listID, $v['email'], $merge_vars,'pending');
                        }
                        else if($user['status']==0){
                            $modelCommon->updateR('mailchimp','status',1,'email="'.$v['email'].'"');//in the case of an existent user - updates the db
                            $status = \Yii::$app->mailchimp->updateMember($listID, $v['email'], $merge_vars,'pending');
                        }
                            

                            if(is_array($status)){
                                if(array_key_exists('status', $status)){
                                    if(in_array($status['status'], array('pending','subscribed','pending'))){
                                        $status = true;
                                    }
                                }
                            }
                            else{
                                array_push($err_mailchimp,array($status));
                            }
                    }
                    else{//string with all invalid email addresses 
                        $err_txt .=$v['email']. PHP_EOL;
                    }
                    unset($status);
                }

//               // saves all new email addresses into db
                $modelCommon->multipleSave('mailchimp', $fields, $values);

                // logs errors
                $modelCommon->writeFile($file_err_save, $err_txt);
                $modelCommon->writeFile($file_err_mailchimp, json_encode($err_mailchimp));
            }// end if "array not empty"
            else{//writes error logs
                $modelCommon->writeFile($file_err_save, 'No JSON data');
                $modelCommon->writeFile($file_err_mailchimp, 'No JSON data');
            }
        }// end try "no json files"
        catch (\yii\base\ErrorException $e){// writes error logs
            $modelCommon->writeFile($file_err_save, $e->getMessage());
            $modelCommon->writeFile($file_err_mailchimp, $e->getMessage());
        }
    }
    
    /**
     * Unsubscribes users from a newsletter
     */

    public function actionUnsubnewsletter(){
        $modelCommon = new Common();
        $values = array();
        $err_txt ="Invalid email addresses: ". PHP_EOL;
        $e = false;
        $merge_vars = array('FNAME' =>'-', 'LNAME' =>'-');
        $filepath = '/var/www/html/mailchimp_yii_console/console';//local
        $file_err_unsubnwsl = $filepath.'/logs/err_unsubnewsl.txt';
        $file_err_mailchimp_unsubnwsl = $filepath.'/logs/err_mailchimp_unsubnewsl.txt';
        $file_json = $filepath.'/data/data.json';
        $status ='';
        $err_mailchimp_unsubnwsl = array();
        $json_e_a=array();
        
        $ls = \Yii::$app->mailchimp->getLists();
        $listID = ($ls['lists'][0]['id']);// typically, it's the first one, if you have only one campaign on MailChimp or your apikey is referred at a specific campaign.
               
               
        try{//checks if JSON files are written
            $string = file_get_contents($file_json);
            $json_a = json_decode($string, true);
            $nwsl_db = $modelCommon->getAllItems('mailchimp');
            
            if(is_array($json_a) && !empty($json_a)){// runs only if the array is not empty
                foreach($json_a['users'] as $kk=>$vv){
                    $json_e_a[] = $vv['email'];
                }
            }
            
            if(is_array($nwsl_db) && !empty($nwsl_db)){// runs only if the array is not empty
                foreach($nwsl_db as $k=>$v){
                    if (filter_var($v['email'], FILTER_VALIDATE_EMAIL)) {
                        if(!in_array($v['email'], $json_e_a)){// if user's email address isn't in the json file, update it to unsubscribed 
                            $modelCommon->updateR('mailchimp','status',0,'email="'.$v['email'].'"');

                            $status = \Yii::$app->mailchimp->updateMember($listID, $v['email'], $merge_vars,'unsubscribed');

                            if(is_array($status)){
                                if(array_key_exists('status', $status)){
                                    if(in_array($status['status'], array('pending','subscribed','pending'))){
                                        $status = true;
                                    }
                                }
                            }
                            else{
                                array_push($err_mailchimp_unsubnwsl,array($status));
                            }
                        }
                    }
                    else{//string with all invalid email addresses 
                        $err_txt .=$v['email']. PHP_EOL;
                    }
                    unset($status);
                }

                // logs errors
                $modelCommon->writeFile($file_err_unsubnwsl, $err_txt);
                $modelCommon->writeFile($file_err_mailchimp_unsubnwsl, json_encode($err_mailchimp_unsubnwsl));
            }// end if "array not empty"
            else{//writes error logs
                $modelCommon->writeFile($file_err_unsubnwsl, 'No JSON data');
                $modelCommon->writeFile($file_err_mailchimp_unsubnwsl, 'No JSON data');
            }
        }// end try "no json files"
        catch (\yii\base\ErrorException $e){// writes error logs
            $modelCommon->writeFile($file_err_unsubnwsl, $e->getMessage());
            $modelCommon->writeFile($file_err_mailchimp_unsubnwsl, $e->getMessage());
        }
        
    }
}