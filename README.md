# MailChimp, JSON, bash and Yii2 Console App

## Abstract 
To manage Mailchimp actions and import JSON data into a MySQL/MariaDb database through YII2 Console, bash and crontab

## Premise
This project requires :

* PHP 7.x
* MySql/MariaDb 5.x
* Composer

They have to be up and running. 

## Installation
*  Download and install Yii2 framework(advanced template), via composer, on your local computer . For all info click [here](https://yiiframework.com) 
 *  The root folder of the project is 
 
       >**mailchimp_yii_console** 
    
 * and the absolute path is
 
      >**/var/www/html/yiiconsole/mailchimp_yii_console**
    
  * Clone the project from gitlab on Desktop :
    
    >**git@gitlab.com:greengo_axe/mailchimp_yii_console.git**

* copy and overwrite all folders and files from 

>>**Desktop/mailchimp_yii_console**

>into 

>>**/var/www/html/yiiconsole/mailchimp_yii_console**

* Delete the folder on Desktop (**Desktop/mailchimp_yii_console**)

* Change the permissions:
> **cd /var/www/html/yiiconsole/mailchimp_yii_console**
> 
> **chmod 777 yii**

* Import the database structure (**mailchimp_yii_console/mcconsole.sql**) into MySQL/MariaDb via PHPMyAdmin or via console

> /var/www/html/yiiconsole/mailchimp_yii_console/common/config/main-local.php # db settings

* Insert MailChimp api key into:

 > /var/www/html/yiiconsole/mailchimp_yii_console/console/config/main.php # db settings

* Set the crontab:

> **crontab -e**

> **\*/1 * * * \* /usr/bin/bash /var/www/html/yiiconsole/mailchimp_yii_console/console/scripts/process_mailchimp.sh** #every minute

* Enjoy!

## Troubleshooting
This project has been developed on Linux OS and there might be a few problems with the path or to grant the right permissions on Windows / Apple OSs.

* You're free to change the path of the project. If so, you have to replace the new path in the following files:

>1. console/controllers/**McController.php**

>1. console/scripts/**process_mailchimp.sh**

>1. console/scripts/**process_mailchimp_unsub.sh**

* The direct commands of Yii2 Console are:

>> **/path/to/folder/mailchimp_yii_console/yii mc/savetodb** # to save data into the db and subscribe the user into the campaign on MailChimp

>> **/path/to/folder/mailchimp_yii_console/yii mc/unsubnewsletter** # to update data into the db (status =0) and unsubscribe the user(not deletion) into the campaign on MailChimp

>Ex.:

>>**/var/www/html/yiiconsole/mailchimp_yii_console/yii mc/savetodb**

>>**/var/www/html/yiiconsole/mailchimp_yii_console/yii mc/unsubnewsletter**

